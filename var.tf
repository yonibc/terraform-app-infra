##################################################################################
# VARIABLES
##################################################################################

#Application Variables#
variable "key_name" {
  default = "terraform-key"
}

#VPC Variables#
variable "network_address_space" {
  description = "The VPC CIDR Block Range"
}
variable "subnet1_address_space" {
  description = "Subnet 1 IP range"
}
variable "subnet2_address_space" {
  description = "Subnet 2 IP range"
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
}


#EC2 Instance Variables#
variable "ec2_count" {
  description = "Number of EC2 instances"
}

variable "instance_type" {
  description = "The type of EC2 instances to run (e.g. t2.micro)"
}
