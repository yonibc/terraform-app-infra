##################################################################################
# OUTPUT
##################################################################################

#EC2 Instance Public DNS
output "aws_atlantis_public_dns" {
    value = "${aws_instance.atlantis.*.public_dns}"
}

#Load Balancer Public DNS
output "aws_elb_public_dns" {
    value = "${aws_elb.web.dns_name}"
}
