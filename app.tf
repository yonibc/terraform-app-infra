##################################################################################
# RESOURCES
##################################################################################

#Creates EC2 Instances for applications#
resource "aws_instance" "atlantis" {
  ami = "ami-759bc50a"
  count = "${var.ec2_count}"
  instance_type = "${var.instance_type}"
  subnet_id = "${aws_subnet.subnet1.id}"
  vpc_security_group_ids = ["${aws_security_group.web_access.id}"]
  key_name = "${var.key_name}"

  user_data = <<-EOF
              #!/bin/bash
              echo "Atlantis has pushed our code and deployed our infrastructure.... YAAAY!" > index.html
              nohup busybox httpd -f -p "80" &
              EOF

  tags {
    Name = "${var.cluster_name}"
  }

}
